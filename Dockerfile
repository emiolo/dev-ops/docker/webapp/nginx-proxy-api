FROM nginx:alpine

ENV SSH_PASSWD "root:Docker!"
ENV SSH_PORT 2222

RUN apk add --no-cache openssh-server openrc \
 && echo "$SSH_PASSWD" | chpasswd \
 && rc-update add sshd

RUN mkdir -p /home/site/nginx/conf.d

COPY ./conf.d/default.conf /etc/nginx/conf.d/default.conf
COPY ./init_container.sh /usr/bin/

EXPOSE 80
EXPOSE 2222

WORKDIR /home/site/nginx/conf.d

ENTRYPOINT /usr/bin/init_container.sh
