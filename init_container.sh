#!/bin/sh

eval $(printenv | sed -n "s/^\([^=]\+\)=\(.*\)$/export \1=\2/p" | sed 's/"/\\\"/g' | sed '/=/s//="/' | sed 's/$/"/' >> /etc/profile)
sed -i "s/SSH_PORT/$SSH_PORT/g" /etc/ssh/sshd_config

rc-service sshd start

/usr/sbin/nginx -g "daemon off;"
